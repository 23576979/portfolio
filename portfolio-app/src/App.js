import React from 'react';
import './App.scss';
import Routes from './Routes/Routes';


function App() {
  return (
    <div className="App">
      <Routes baseurl='/'/>
    </div>
  );
}

export default App;
