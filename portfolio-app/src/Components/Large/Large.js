import React from 'react';
import '../Small/Small.scss';
import NavBar from '../Navbar/Navbar';
import BlueShape from '../BlueShape/BlueShape';
import Phase2 from '../../assets/doggiewalksdesign.png';
import Phase3 from '../../assets/doggiewalksprod.png';
import Phase1 from '../../assets/doggieWalks-home.jpg';

class Large extends React.Component{
  render(){
    return(
      <section id='large' className='briefPage'>
        <h2 data-aos="fade-right" className='title'>Doggie Walks</h2>
        <section className='briefTimeline'>
          <section data-aos="fade-down-right" data-aos-delay='1000' className='phase phase-1'>
            <h3 className='phaseTitle'>Phase 1</h3>
            <img className='phaseImage' src={Phase1}/>
            <p className='phaseDesc'>In the first stage of design, using the client requirements and specifications, low-fideltiy prototypes are created using pen and paper. These are very rough sketches that are often repeated and refined until the designer/developer and the client are happy with the layout and structure of the page. Content such as images/text and colours are not added at this stage.</p>
          </section>
          <section data-aos="fade-down-left" data-aos-delay='1500' className='phase phase-2'>
            <h3 className='phaseTitle'>Phase 2</h3>
            <img className='phaseImage' src={Phase2}/>
            <p className='phaseDesc'>Once a low-fidelity prototype design has been agreed upon, the designer can move on to the next phase, whereby the paper drawings are taken and transformed into high-fidelity prototypes that more accuratly represent what the site will look like. This is the stage where colours, fonts, images and text content are added to the page. The designs went back and forward between myself and the client to refine the design until the client is happy. The final updated design can be seen in the image above.</p>
          </section>
          <section data-aos="fade-down-right" data-aos-delay='1800' className='phase phase-3'>
            <h3 className='phaseTitle'>Phase 3</h3>
            <a href='https://sleepy-pike-5ea695.netlify.app/'>
            <img className='phaseImage' src={Phase3}/>
            </a>
            <p className='phaseDesc'>In the final stage of this brief, once the final design has been cleared and any changes that the client has brought forward have been implemented, this is where the developer will take the high-fid prototype and create the site in code.</p>
          </section>
        </section>
      </section>
    )
  }
}
export default Large