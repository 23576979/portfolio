import React from 'react';
import './Navbar.scss';
import Logo from '../../assets/Logo-Single-Trans.png';

class Navbar extends React.Component{
  constructor(props){
    super(props);
    this.myRef = React.createRef();
  }
  openNav = () => {
    this.myRef.current.style.width = '80px';
    this.myRef.current.style.position = 'fixed';
    this.props.home.current.style.marginLeft = '100px';
  }
  closeNav = () => {
    this.myRef.current.style.width = '0px';
    this.myRef.current.style.position = 'relative';
    this.props.home.current.style.marginLeft = '20px';
  }
  render(){
    return (
      <section>
        <i onClick={this.openNav} className="hamburger fas fa-bars"></i>
      <header data-aos="fade-right" data-aos-once="true" ref={this.myRef}>
        <nav>
          <section>
            <span className='x'onClick={this.closeNav}>X</span>
            <img className='logo' src={Logo}/>
            <ul className='navItems'>
              <li className='active'><a href='#home'>Home</a></li>
              <li><a href='#about-me'>About<br/>Me</a></li>
              <li><a href='#briefs'>Breifs</a></li>
            </ul>
          </section>
          <section>
            <ul className='socialIcons'>
              <li><a href='https://codepen.io/JakeJoy' target='blank'><i className="fab fa-codepen"></i></a></li>
              <li><a href='https://www.linkedin.com/in/jake-leigh-265685147/' target='blank'><i className="fab fa-linkedin-in" target='blank'></i></a></li>
              <li><a href='https://github.com/JakeLeigh' target='blank'><i className="fab fa-github"></i></a></li>
              <li><a href='mailto:jake@jakeleigh.io'><i className="far fa-envelope"></i></a></li>
            </ul>
          </section>
          {/* <div className="theme-switch-wrapper">
            <label className="theme-switch" for="checkbox">
              <input onChange={this.changeColours} type="checkbox" id="checkbox" />
              <div className="slider round"></div>
            </label>
          </div> */}
        </nav>
      </header>
      </section>
    )
  }
}

export default Navbar; 