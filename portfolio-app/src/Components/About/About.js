import React from 'react';
import photo from '../../assets/selfPhoto.png';
import './About.scss';

class About extends React.Component{
  render(){
    return (
      <section id='about-me' className='aboutPage'>
        <h2 data-aos="fade-right" className='title'>About me</h2>
        <section className='aboutContent'>
          <img data-aos="zoom-in" data-aos-delay='500' className='selfPhoto' src={photo}/>
          <section className='yearContent'>
          <h3 data-aos="fade-left" data-aos-delay='1000' className='year'>2017</h3>
          <hr data-aos="fade-left" data-aos-delay='1000'/>
          <p data-aos="fade-left" data-aos-delay='1000'className='text'>In 2017, I completed a two year level 3 BTEC course in software development at Peterborough Regional College. Over the two years, I learnt a wide variety of areas within software development including; 3D modelling, database design and management, low-level programming and web design and developement.</p>
          <p data-aos="fade-left" data-aos-delay='1000'className='text'>Towards the end of my final year at college, we had to complete six weeks of work experience at a company within the industry we were intersted in. I found a small, start-up web design company and asked to work there for six weeks, the experience was very enlightening as it was my first experience in working within the web industry. </p>
          <h3 data-aos="fade-right" data-aos-delay='1500'className='year'>2019</h3>
          <hr data-aos="fade-right" data-aos-delay='1500'/>
          <p data-aos="fade-right" data-aos-delay='1500'className='text'>During the summer of 2019 after just completing my second year of a BSc degree in Web design and development at Edge Hill University. I completed an internship at Monmia as a front-end web developer, for 3 months where at the end of the internship, they offered me a full time position post univeristy which I gladly accepted based on results and other offers.</p>
          <p data-aos="fade-right" data-aos-delay='1500'className='text'>Currently at the end of my final year of university, completing my dissertation. I am still planning on working full-time at Monmia once I finish university.</p>
          </section>
        </section>
      </section>
    )
  }
}
export default About;