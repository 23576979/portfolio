import React from 'react';
import './Briefs.scss';
import Small from '../../assets/small.png';
import Large from '../../assets/Large.png';
import {Link} from 'react-router-dom';

class Briefs extends React.Component{
  render(){
    return (
      <section id='briefs' className='breifsPage'>
      <h2 data-aos="fade-right" className='title'>Briefs</h2>
      <section data-aos="zoom-out-up" data-aos-delay='1000' className='aboutContent'>
        <a href='#small'>
        <section className='briefDesc'>
          <h2 className='briefTitle'>Responsive Website Tester</h2>
          <p className='text'>A small brief where the requirements are to be able to compare multiple device sizes using iframes.</p>
        </section>
        </a>
        <a href='#small'>
          <img className='briefImage' src={Small}/>
        </a>
      </section>
      <br />
      <section data-aos="zoom-out-up" data-aos-delay='1500' className='aboutContent'>
        <a href='#large'>
        <section className='briefDesc'>
          <h2 className='briefTitle'>Doggie Walks</h2>
          <p className='text'>A large brief for a client looking for a new dog walk company, using the spec list and <br className='showOnDesktop'/>client brief to successfully build a modern and functional website.</p>
        </section>
        </a>
        <a href='#large'>
          <img className='briefImage' src={Large}/>
        </a>
      </section>
    </section>
    )
  }
}

export default Briefs;