import React from 'react';
import './Arrow.scss';
import {Link} from 'react-router-dom';

class Arrow extends React.Component{
  render(){
    return (
      <section className='downArrow'>
      <Link to={this.props.goTo}><i  data-aos="zoom-out-up" class="arrow fas fa-arrow-down"></i></Link>
      </section>
    )
  }
}
export default Arrow;