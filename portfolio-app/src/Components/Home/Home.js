import React from 'react';
import './Home.scss';
import BlueShape from '../BlueShape/BlueShape';
import NavBar from '../Navbar/Navbar';
import About from '../About/About';
import Briefs from '../Briefs/Briefs';
import Arrow from '../Arrow/Arrow';
import Small from '../Small/Small';
import Large from '../Large/Large';
import AOS from 'aos';
import '../../aos/dist/aos.css';

class Home extends React.Component{
  constructor(props){
    super(props);
    this.home = React.createRef();
  }
  componentWillMount(){
    AOS.init({
      duration : 1000,
      mirror: true
    })
  }
  render(){
    return (
      <section>
        <NavBar home={this.home}/>
        <BlueShape/>
        <section className='body' ref={this.home}>
          <section id='home' className='homeSplash' data-aos="fade-up-right" data-aos-delay="1000">
            <h1 className='splashTitle'>Hi, I'm Jake</h1>
            <h2 className='splashSub'>A web design and development student at Edge Hill<br className='showOnDesktop'/> University, and a front-end React developer at Monmia.</h2>
            <a href='#about-me'><button className='splashBtn'>Learn More</button></a>
          </section>
          <Arrow goTo='#about-me'/>
          <About />
          <Arrow goTo='#breifs'/>
          <Briefs />
          <Arrow/>
          <Small/>
          <Arrow/>
          <Large />
        </section>
      </section>
    )
  }
}
export default Home;