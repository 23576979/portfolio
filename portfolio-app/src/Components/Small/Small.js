import React from 'react';
import './Small.scss';
import NavBar from '../Navbar/Navbar';
import BlueShape from '../BlueShape/BlueShape';
import Phase2 from '../../assets/small-first-design.png';
import Phase3 from '../../assets/smallPrototype.png';
import Phase4 from '../../assets/smalldeployed.png';
import Phase1 from '../../assets/responsive-main.jpg';
import {Link} from 'react-router-dom';

class Small extends React.Component{
  render(){
    return(
      <section id='small' className='briefPage'>
        <h2 data-aos="fade-right" className='title'>Responsive Website Tester</h2>
        <section className='briefTimeline'>
          <section data-aos="fade-down-right" data-aos-delay='1000' className='phase phase-1'>
            <h3 className='phaseTitle'>Phase 1</h3>
            <img className='phaseImage' src={Phase1} alt='drawing of original design'/>
            <p className='phaseDesc'>In the first stage of design during this brief, the written requirements provided by the client were analysed for features needed as well as stylistic elements. Once the requirements were understood, low-fidelity mock-ups were created just using pen and paper, as shown in the image above.</p>
          </section>
          <section data-aos="fade-down-left" data-aos-delay='1500' className='phase phase-2'>
            <h3 className='phaseTitle'>Phase 2</h3>
            <img className='phaseImage' src={Phase2}/>
            <p className='phaseDesc'>Once the low-fidelity prototype had been completed, the next phase consisted of turning the prototype into a high fidelity design using Figma. This design is used to illustrate to the designer/developer as well as the client what the site or application will look like, this is due to the fact that at this stage,colours, fonts, images and text content are added as well. </p>
          </section>
          <section data-aos="fade-down-right" data-aos-delay='2000' className='phase phase-3'>
            <h3 className='phaseTitle'>Phase 3</h3>
            <img className='phaseImage' src={Phase3}/>
            <p className='phaseDesc'>The next stage consisted of showing the designs and mock-ups to the client for them to either give the green light to move to development, or to voice any concerns or changes they would implement. The client did voice a concern that having a single iframe that changed size for each device was not completely meeting the brief as the brief outlines the functionality to compare multiple iframes at different sizes. Therefore with some new requirements put in place, I went back to phase 2 and re-designed the fidelity prottp to contain two iframes and a drop-down selector to select different device sizes for each iframe. Then the designs were brought back to the client where they accepted the designs and gave me the green light to begin development.</p>
          </section>
          <section data-aos="fade-down-left" data-aos-delay='2000' className='phase phase-4'>
            <h3 className='phaseTitle'>Phase 4</h3>
            <a href='https://modest-benz-bd6363.netlify.app/'>
            <img className='phaseImage' src={Phase4}/>
            </a>
            <p className='phaseDesc'>In the final phase, development began using the updated high fidelity designs as a guide to an identical representation of the design.</p>
          </section>
        </section>
      </section>
    )
  }
}
export default Small