import React from 'react';
import Home from '../Components/Home/Home';
import {Switch, Route, BrowserRouter as Router, BrowserRouter} from 'react-router-dom';

const Routes = props => {
  return (
    <Router history={BrowserRouter}>
      <Switch>
        <Route path='/' component={Home}/>
      </Switch>
    </Router>
  )
}

export default Routes;